brox: main.cpp
	$(CXX) $^ -o $@ \
		-std=c++14 -Wall -O3 \
		-lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_gpu \
	 	-lboost_filesystem -lboost_system -lboost_program_options

.PHONY: clean
clean:
	-rm brox
