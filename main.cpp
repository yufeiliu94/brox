#include "opencv2/opencv.hpp"
#include "opencv2/gpu/gpu.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "boost/filesystem.hpp"
#include "boost/program_options.hpp"

#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <chrono>
#include <fstream>
#include <iterator>
#include <memory>
#include <vector>
#include <algorithm>

namespace fs = boost::filesystem;
namespace po = boost::program_options;

/**
 * 运行时程序参数
 */
struct RuntimeOptions {
  float alpha = 0.197;
  float gamma = 50;
  float scale = 0.8;
  int gpu_id = 0;
  std::string name_format = "%06d.jpg";
  std::string in;
  std::string out;
};

std::ostream& operator<<(std::ostream& os, RuntimeOptions &ro) {
  os << "Reporting options" << std::endl;
#define STRINGIFY(x) #x
#define PUT(what) os << "  " STRINGIFY(what) " = " << ro.what << std::endl
    PUT(alpha);
    PUT(gamma);
    PUT(scale);
    PUT(gpu_id);
    PUT(in);
    PUT(out);
#undef PUT
#undef STRINGIFY
  return os;
}

/**
 * 兼容帧文件夹 / 视频的序列读取器
 */
class FrameSequenceReader {
  public:
    /**
     * 包含RGB帧序列的文件夹 / 视频文件
     */
    FrameSequenceReader(const std::string name) {
      if (not fs::is_directory(name)) {
        vc.reset(new cv::VideoCapture(name));
      } else {
        fs::directory_iterator iter(name);
        fs::directory_iterator iend;
        while (iter != iend) {
          frames.push_back(std::string((*iter++).path().c_str()));
        }
        std::sort(frames.begin(), frames.end());
      }
    }

    bool read(cv::Mat &dest) {
      if(vc.get() != nullptr) {
        return vc->read(dest);
      } else {
        if (counter < frames.size()) {
          dest = cv::imread(frames[counter++]);
          return true;
        } else {
          return false;
        }
      }
    }
  private:
    std::unique_ptr<cv::VideoCapture> vc;
    std::vector<std::string> frames;
    size_t counter = 0;
};


/**
 * 格式化输出文件名
 */
static inline std::string build_name(size_t frame_serial) {
  static char buffer[32];
  sprintf(buffer, "%06d.jpg", int(frame_serial));
  return buffer;
}

static inline void grayscale(const cv::Mat &src, cv::Mat &dist) {
  cv::Mat tmp;
  src.convertTo(tmp, CV_32F, 1.0 / 255.0);
  cv::cvtColor(tmp, dist, CV_BGR2GRAY);
}

static inline void visualization(const cv::gpu::GpuMat &u, const cv::gpu::GpuMat &v, cv::Mat &img) {
  const float scale = 1.0;
  cv::gpu::GpuMat mag, su, sv, nu, nv, s128, u2pv2;
  cv::gpu::GpuMat s1 = cv::gpu::GpuMat(cv::Mat::ones(u.size(), CV_32FC1));
  cv::gpu::multiply(s1, 0.5, s128);
  cv::gpu::normalize(u, nu);
  cv::gpu::normalize(v, nv);
  cv::gpu::pow(nu, 2, su);
  cv::gpu::pow(nv, 2, sv);
  cv::gpu::add(su, sv, u2pv2);
  cv::gpu::sqrt(u2pv2, mag);
  cv::gpu::multiply(mag, scale, mag);
  cv::gpu::add(mag, s128, mag);
  // u/v <- [0, 255]
  cv::gpu::multiply(nu, scale, su);
  cv::gpu::add(su, s128, su);
  cv::gpu::min(su, 1, su);
  cv::gpu::max(su, 0, su); 
  cv::gpu::multiply(nv, scale, sv);
  cv::gpu::add(sv, s128, sv);
  cv::gpu::min(sv, 1, sv);
  cv::gpu::max(sv, 0, sv);
  // marge
  std::vector<cv::gpu::GpuMat> bgr_channels {su, sv, mag};
  cv::gpu::merge(bgr_channels, s1);
  s1.download(img);
  img.convertTo(img, CV_8UC3, 255);
}

int parse_options(int argc, char *argv[], RuntimeOptions &opt) {
  po::options_description desc("Brox Optical Flow Calculator Options");
  desc.add_options()
    ("help", "print this message")
    ("alpha", po::value<float>(&opt.alpha)->default_value(0.197), "flow smoothness")
    ("gamma", po::value<float>(&opt.gamma)->default_value(50),    "radient constancy importance")
    ("scale", po::value<float>(&opt.scale)->default_value(0.8),   "pyramid scale factor")
    ("gpu",   po::value<int>(&opt.gpu_id)->default_value(0),      "GPU Device ID")
    ("format", po::value<std::string>(&opt.name_format)->default_value("%06d.jpg"), "output frame name format")
    ("in", po::value<std::string>(&opt.in), "source")
    ("out", po::value<std::string>(&opt.out), "outpur dir");
  po::positional_options_description positionalOptions; 
  positionalOptions.add("in", 1); 
  positionalOptions.add("out", 1); 
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(positionalOptions).run(), vm);
  po::notify(vm);
  if (vm.count("help") != 0 or vm.count("in") != 1 or vm.count("out") != 1) {
    std::cout << desc << std::endl;
    return 1;
  }
  return 0;
}

int main(int argc, char *argv[]) {
  auto start = std::chrono::high_resolution_clock::now();
  RuntimeOptions opt;
  if (parse_options(argc, argv, opt) == 0) {
    std::cout << opt;
  } else {
    return 0;
  }
  cv::gpu::setDevice(opt.gpu_id);
  cv::gpu::BroxOpticalFlow optflow(opt.alpha, opt.gamma, opt.scale, 10, 150, 10);
  cv::Mat prev, curr, pg, cg, u_cpu, v_cpu, of;
  size_t frame_counter = 0;
  FrameSequenceReader cap(opt.in);
  cap.read(prev);
  prev.copyTo(of);
  cv::Mat cmp;
  cv::gpu::GpuMat f0, f1, u, v;
  grayscale(prev, pg);
  f0.upload(pg);
  while (cap.read(curr)) {
    grayscale(curr, cg);
    f1.upload(cg);
    optflow(f0, f1, u, v);
    visualization(u, v, of);
    frame_counter += 1;
    cv::imwrite(opt.out + "/" + build_name(frame_counter), of);
    cg.copyTo(pg);
    f1.copyTo(f0);
  }
  auto finish = std::chrono::high_resolution_clock::now();
  auto ms = std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count() / 1000000;
  std::cout << frame_counter << " frames in " << ms << " ms, FPS = " << 1000.0 * frame_counter / ms << std::endl;
  return 0;
}
