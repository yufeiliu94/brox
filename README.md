# Brox光流算法

## 依赖

+ GCC 5或更高版本
+ Boost 1.58或更高，依赖模块
    + FileSystem
    + System
    + ProgramOptions
+ 开启GPU支持的OpenCV 2.4

## 构建

```shell
git clone https://gitlab.com/yufeiliu94/brox.git
git checkout GPU
make
```

## 使用方法

### 实例用法

处理视频文件`foo.avi`，将结果写到`bar/`文件夹中，**文件夹`bar`必须存在**。
```shell
./brox foo.avi bar
```

处理`foo/`中的图像序列，将结果写到`bar/`文件夹中，**文件夹`bar`必须存在**。
```shell
./brox foo bar
```

### 命令行参数

使用
```shell
./brox --help
rox Optical Flow Calculator Options:
  --help                     print this message
  --alpha arg (=0.196999997) flow smoothness
  --gamma arg (=50)          radient constancy importance
  --scale arg (=0.800000012) pyramid scale factor
  --gpu arg (=0)             GPU Device ID
  --format arg (=%06d.jpg)   output frame name format
  --in arg                   source
  --out arg                  outpur dir
```
其中`alpha`、`gamma`、`scale`是Brox算法的选项；`gpu`用于指定GPU的设备ID；
`format`用于指定输出图像的文件名，
格式化字符串中至多包含一个整数占位符用以表示当前帧的编号，编号从1开始。